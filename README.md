# The MEAN Artoo #

MEAN.js code container for the Artoo Campus. Ancona - 2015.

## How to contribute to develop ##
1. Forka questo repo (una tantum).

1. Sincronizza il tuo repo con *weboxstudio/artoo-campus* utilizzando la funzione "compara" su bitbucket. Ricordati di effettuare la sincronizzazione da develop a develop.

1. Effettua il checkout in un nuovo branch digitando in console:

    ```
    git checkout -b feature-branch develop
    ```

1. Aggiungi il tuo contributo al codice.

1. Esegui il commit del tuo contributo digitando in console:

    ```
    git add -A
    ```

    ```
    git commit -m "Scrivi qui la descrizione del tuo commit."
    ```

1. Pusha il tuo commit sul repo remoto digitando in console:

    ```
    git push origin nuovo-branch
    ```

1. Crea una pull request su bitbucket da *tuo-user/artoo-campus/feature-branch* a *weboxstudio/artoo-campus/develop*

## Contributors ##
* David Sorrentino